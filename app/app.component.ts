import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location, LocationStrategy, PlatformLocation } from '@angular/common';

import { AuthenticationService } from './authentication.service';
import { User } from './user.model';

@Component({
	selector: 'my-app',
	templateUrl: 'app/app.component.html',
	styleUrls: [ 'app/app.css' ]
})

export class AppComponent {
	model: User = new User('', '');

	constructor(private authService: AuthenticationService) {}
}
